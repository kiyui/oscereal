{ ... }: {
  hardware.bluetooth.enable = true;
  hardware.bluetooth.settings = {
    General = { ControllerMode = "bredr"; }; # for compatibility with airpods
    Policy = { AutoEnable = "true"; };
  };
}
