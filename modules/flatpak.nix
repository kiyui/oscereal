{ config, lib, pkgs, ... }: {
  services.flatpak.enable = true;

  # include flatpak-builder if the core gnome developer tools are enabled
  environment.systemPackages = with pkgs;
    lib.optionals config.services.gnome.core-developer-tools.enable
    [ flatpak-builder ];
}
