{ config, lib, pkgs, ... }:
# We hackily detect if this is a VM build and disable snapper if true
let
  isVMBuild = (lib.hasAttr "/tmp/xchg" config.fileSystems)
    && config.fileSystems."/tmp/xchg".fsType == "9p";
in lib.mkIf (!isVMBuild) {
  assertions = [{
    assertion = config.fileSystems."/home".fsType == "btrfs";
    message = "The `/home` filesystem must be `btrfs`";
  }];

  services.snapper.cleanupInterval = "12h";
  services.snapper.snapshotInterval = "hourly";
  services.snapper.configs.home = {
    SUBVOLUME = "/home";
    ALLOW_USERS = [ "dafne" ];
    TIMELINE_CREATE = true;
    TIMELINE_CLEANUP = true;
  };

  environment.systemPackages = with pkgs;
    lib.optionals config.services.xserver.enable [ snapper-gui ];
}
