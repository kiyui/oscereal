{ pkgs, ... }: {
  time.timeZone = "Asia/Kuala_Lumpur";

  i18n.defaultLocale = "en_GB.UTF-8";
  i18n.inputMethod = {
    enabled = "ibus";
    ibus.engines = with pkgs.ibus-engines; [ uniemoji libpinyin ];
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    noto-fonts-extra
    nerdfonts
  ];
}
