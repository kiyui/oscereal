{ config, lib, pkgs, modulesPath, ... }: {
  # behemoth hardware
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ehci_pci" "ahci" "nvme" "usb_storage" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.initrd.luks.devices."cryptroot".device =
    "/dev/disk/by-uuid/ba89bde6-f915-4cfa-bcf0-8183610e2042";
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/7702-4DEA";
    fsType = "vfat";
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/2397e710-b109-411c-ab8e-f50990b31d0c";
    fsType = "btrfs";
    options = [
      "subvol=@"
      "noatime"
      "compress-force=zstd"
      "space_cache"
      "ssd"
      "discard=async"
    ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/2397e710-b109-411c-ab8e-f50990b31d0c";
    fsType = "btrfs";
    options = [
      "subvol=@home"
      "noatime"
      "compress-force=zstd"
      "space_cache"
      "ssd"
      "discard=async"
    ];
  };

  fileSystems."/home/.snapshots" = {
    device = "/dev/disk/by-uuid/2397e710-b109-411c-ab8e-f50990b31d0c";
    fsType = "btrfs";
    options = [
      "subvol=@home/.snapshots"
      "noatime"
      "compress-force=zstd"
      "space_cache"
      "ssd"
      "discard=async"
    ];
  };

  swapDevices = [{ device = "/swapfile"; }];

  hardware.nvidia.prime = {
    intelBusId = "PCI:0:2:0";
    nvidiaBusId = "PCI:2:0:0";
  };

  hardware.video.hidpi.enable = lib.mkDefault true;

  # behemoth VM options, broken: https://github.com/NixOS/nixpkgs/issues/59219
  # virtualisation.cores = 4;
  # virtualisation.diskSize = 8192;
  # virtualisation.memorySize = 4096;

  # behemoth system
  networking.hostName = "behemoth";

  # behemoth-only programs
  environment.systemPackages = with pkgs; [
    # browsers
    brave
    firefox-wayland
    vivaldi

    # not browsers, apparently
    spotify
    vscode

    # etc
    deja-dup
    protonvpn-gui
  ];

  # special programs
  programs.steam.enable = true;

  # enable oscereal extras
  oscereal.programs.fish.enable = true;
  oscereal.programs.base.enable = true;
  oscereal.programs.extras.enable = true;
  oscereal.programs.programming.enable = true;
  oscereal.programs.devops.enable = true;
  oscereal.programs.hacking.enable = true;
  oscereal.programs.android.enable = true;
  oscereal.programs.office.enable = true;
  oscereal.programs.work.enable = true;
}
