{ config, lib, pkgs, ... }: {
  environment.systemPackages = with pkgs;
    lib.optionals config.services.xserver.enable [ yubioath-flutter ];
}
