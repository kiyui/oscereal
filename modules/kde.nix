{ config, pkgs, ... }: {
  # disable hidpi
  assertions = [{
    assertion = config.hardware.video.hidpi.enable == false;
    message = "HiDPI must be disabled to use KDE";
  }];

  # use plasma/sddm combo
  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # manually enable networkmanager
  networking.networkmanager.enable = true;
}
