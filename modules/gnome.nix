{ pkgs, ... }: {
  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.gnome.core-developer-tools.enable = true;

  # important non-default applications
  environment.systemPackages = with pkgs; [
    # enable heif support for eog & nautilus
    libheif

    # additional apps
    gnome.gnome-tweaks
    gnome.gnome-boxes
  ];
}
