{ config, lib, pkgs, modulesPath, ... }: {
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.bootspec.enable = true;

  boot.initrd.luks.devices."cryptroot".device =
    "/dev/disk/by-uuid/fce77d59-3648-4720-b5f2-4dba6a4edeae";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/b906b503-c3b7-4956-a0bd-ef6eee215ed0";
    fsType = "btrfs";
    options = [ "subvol=@" "noatime" "compress=zstd" "ssd" "discard=async" ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/b906b503-c3b7-4956-a0bd-ef6eee215ed0";
    fsType = "btrfs";
    options =
      [ "subvol=@home" "noatime" "compress=zstd" "ssd" "discard=async" ];
  };

  fileSystems."/swap" = {
    device = "/dev/disk/by-uuid/b906b503-c3b7-4956-a0bd-ef6eee215ed0";
    fsType = "btrfs";
    options = [ "subvol=@swap" "ssd" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/3293-9B61";
    fsType = "vfat";
    options = [ "fmask=0077," "dmask=0077" "defaults" ];
  };

  swapDevices = [{ device = "/swap/swapfile"; }];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp0s20f3.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;

  # jata system
  networking.hostName = "jata";

  # jata-only programs
  environment.systemPackages = with pkgs; [ vivaldi deja-dup protonvpn-gui ];

  # special programs
  programs.steam.enable = true;

  # enable oscereal extras
  oscereal.programs.fish.enable = true;
  oscereal.programs.base.enable = true;
  oscereal.programs.extras.enable = true;
  oscereal.programs.programming.enable = true;
  oscereal.programs.hacking.enable = true;
}
