{
  inputs = {
    # packages
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    lanzaboote.url = "github:nix-community/lanzaboote";
  };

  outputs = { self, nixpkgs, nixos-hardware, lanzaboote }:
    let
      # this should be temporary (famous last words)
      nixFlakes = { pkgs, ... }: {
        nix = {
          package = pkgs.nixVersions.latest;
          extraOptions = ''
            experimental-features = nix-command flakes
          '';
        };
      };
    in {
      nixosConfigurations.jata = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          # enable nix flakes
          nixFlakes

          # hardware
          nixos-hardware.nixosModules.common-cpu-intel
          nixos-hardware.nixosModules.common-pc-ssd
          nixos-hardware.nixosModules.lenovo-thinkpad-x1-10th-gen

          # lanzaboote
          lanzaboote.nixosModules.lanzaboote

          # oscereal
          ./modules/jata.nix
          ./modules/bluetooth.nix
          ./modules/dafne.nix
          ./modules/firmware.nix
          ./modules/flatpak.nix
          ./modules/gnome.nix
          ./modules/localisation.nix
          ./modules/nixos.nix
          ./modules/nvidia.nix
          ./modules/pipewire.nix
          ./modules/programs.nix
          ./modules/snapper.nix
          ./modules/ssh.nix
          ./modules/secure-boot.nix
          ./modules/yubikey.nix
          ./modules/fingerprint.nix
        ];
      };

      nixosConfigurations.behemoth = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          # enable nix flakes
          nixFlakes

          # hardware
          nixos-hardware.nixosModules.common-cpu-intel
          nixos-hardware.nixosModules.common-gpu-nvidia
          nixos-hardware.nixosModules.common-pc-ssd

          # oscereal
          ./modules/behemoth.nix
          ./modules/bluetooth.nix
          ./modules/dafne.nix
          ./modules/firmware.nix
          ./modules/flatpak.nix
          ./modules/gnome.nix
          ./modules/localisation.nix
          ./modules/nixos.nix
          ./modules/nvidia.nix
          ./modules/pipewire.nix
          ./modules/programs.nix
          ./modules/snapper.nix
          ./modules/ssh.nix
          ./modules/systemd-boot.nix
          ./modules/yubikey.nix
        ];
      };
    };
}
